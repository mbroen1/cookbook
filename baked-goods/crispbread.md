# Crispbread / Knækbrød

## Ingredients
| Ingredient     | Amount |
|----------------|--------|
| Flaxseeds      | 100g   |
| Sesameseeds    | 100g   |
| Sunflowerseeds | 100g   |
| Pumpkin seeds  | 100g   |
| Oatmeal        | 100g   |
| Flour (wheat)  | 200g   |
| Baking powder  | 1tsp   |
| Salt           | 2.5tsp |
| Rapeseed oil   | 100ml  |
| Water          | 200ml  |

## Steps
1. Mix **all ingredients** into a thick dough.
2. Roll the dough onto a piece of baking paper¹ (~1-2mm thick).
3. Cut the dough into the desired shapes and sizes.
   - _(Optional)_ Sprinkle with **flaky salt** and press it lightly into the dough.
4. Bake in a preheated oven at 180°C for ~20min or until crisp.
5. Let cool completely before use.

## Notes
1. Rolling the dough between two pieces of baking paper prevents it from sticking to the rolling pin.
