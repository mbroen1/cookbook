# Brownie

## Ingredients

| Ingredient     | Amount         |
|----------------|----------------|
| Butter         | 600g           |
| Dark chocolate | 600g           |
| Egg whites     | 500ml / ~10pcs |
| Sugar          | 800g           |
| Flour (wheat)  | 500g           |
| Vanilla sugar  | 10g            |
| Salt           | 5g             |

## Steps
1. Melt the **butter** and stir in the **chocolate** (chop the chocolate into small chunks if necessary).
2. Whip the **egg whites** and **sugar** until white and airy.
3. Stir the chocolate/butter into the egg/sugar mixture.
4. Sift the **flour**, **vanilla sugar**, and **salt** and fold it into the mixture.
5. Grease a baking tin and line it with baking paper.
6. Pour the dough into the tin. The dough should have a depth of ~3cm.
7. Bake in a preheated oven at 180°C for ~20 minutes.
8. Let cool completely before use.
