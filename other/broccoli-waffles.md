# Broccoli Waffles with Shredded Chicken, Greens, and Cottage Cheese

## Waffles

### Ingredients
| Ingredient             | Amount         |
|------------------------|----------------|
| Broccoli               | 300g           |
| Wholegrain spelt flour | 200g           |
| Salt                   | 3 tsp          |
| Baking powder          | 2 tsp          |
| Milk                   | 3.5 dl         |
| Egg                    | 3 pcs          |
| Grated mozzarella      | 100g           |

### Steps
1. Mix **flour**, **salt**, and **baking powder** in a bowl, then stir in the **milk**.
2. Finely chop the **broccoli** (use a food processor if available), discard the stems.
3. Add **grated mozzarella**, **eggs**, and **broccoli** to the dough.
4. Bake the dough in a preheated waffle iron until golden brown (~ 2-3 minutes).
5. Keep the waffles hot in a preheated oven (~100 °C).

## Shredded Chicken

### Ingredients
| Ingredient             | Amount         |
|------------------------|----------------|
| Chicken breast         | 900g           |
| Olive oil              | 1 dl           |
| Paprika                | 4 tbsp         |
| Salt                   | 2 tsp          |
| Pepper                 | 2 tsp          |

### Steps
1. Marinate the chicken in **oil**, **paprika**, **salt**, and **pepper** overnight.
2. Fry the **chicken** at high heat for ~1 minute on each side.
3. Lower to medium heat and cook for an additional 5 minutes on each side, or until cooked through.
4. Place the **chicken** on a cutting board and shred it with two forks.
5. Just before serving, place the chicken back in a pan on high heat and cook for another ~4 minutes, stirring frequently. Some of the chicken bits should be crispy, and some still moist.

## Toppings and Assembly

### Ingredients
| Ingredient             | Amount         |
|------------------------|----------------|
| Cabbage                | 0.5 pcs        |
| Bell peppers           | 3 pcs          |
| Cottage cheese         | 450g           |
| Fresh coriander        | ~ a handful    |
| Pumpkin seeds          | ~ 100g         |
| Pickled red onions     | ~ 100g         |

### Steps
1. Divide the **cabbage** into fourths and chop it into fine strips.
2. Cut the **bell peppers** into rough chunks.
3. Toast the **pumpkin seeds** over medium-high heat until fragrant.
4. Cover each **waffle** with a layer of **chicken**.
5. Sprinkle a layer of **cabbage** on top.
6. Add a few globs of **cottage cheese** around the waffle.
7. Garnish with **bell peppers**, **pumpkin seeds**, **pickled red onions**, and a generous sprinkling of **coriander**.

## Notes
* The shredded chicken is for ~4-5 waffles. 
* This waffle recipe produce ~8 waffles.(I like to make a little extra, since they keep well in the fridge and are great with other leftovers).
* Portion size per person ~1-1.5 waffles.
* Can also be made as pancakes if a waffle iron is not available.
